package cn.foxtech.cloud.common.mongo.constants;

public class Constant {
    /**
     * Entity们的表结构信息
     */
    public static final String field_schema_name = "edgeEntitySchema";
    /**
     * edgeEntitySchema表的索引
     */
    public static final String value_schema_index = "table";
    /**
     * 同步时间戳
     */
    public static final String field_timestamp_name = "edgeEntityTimeStamp";
    /**
     * 同步状态标记
     */
    public static final String field_flag_name = "edgeEntityFlag";
}
