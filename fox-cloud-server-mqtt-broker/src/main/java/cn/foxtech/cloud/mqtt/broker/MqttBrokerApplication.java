package cn.foxtech.cloud.mqtt.broker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqttBrokerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqttBrokerApplication.class, args);
    }

}
