package cn.foxtech.cloud.aggregator.service.constants;

public class Constant {
    public static final String field_mode_define = "Define";
    public static final String field_mode_logger = "Logger";

    public static final String field_mode_record = "Record";
    public static final String field_mode_value = "Value";

    public static final String field_mode_config = "Config";
}
